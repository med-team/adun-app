/*
   Project: AdunShell

   Copyright (C) 2008 Michael Johnston
   Author:  Michael Johnston

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA.
*/
#ifndef _ADUN_SHELL_
#define _ADUN_SHELL_

#include <AdunKernel/AdunKernel.h>
#include <AdunKernel/AdunController.h>
#include <AdunKernel/AdunIOManager.h>
#include <AdunKernel/AdunCore.h>

/**
This is a generic controller template
*/

@interface AdunShell: AdController
{
}
@end

#endif
