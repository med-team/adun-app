/* 
   Project: ULFramework

   Copyright (C) 2007 Michael Johnston & Jordi Villa-Freixa

   Author: Michael Johnston

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
 
   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
 
   You should have received a copy of the GNU General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA.
*/
#ifndef _ULFRAMEWORK_
#define _ULFRAMEWORK_

#include <ULFramework/ULFrameworkDefinitions.h>
#include <ULFramework/ULFrameworkFunctions.h>
#include <ULFramework/ULIOManager.h>
#include <ULFramework/ULDatabaseInterface.h>
#include <ULFramework/ULAnalysisManager.h>
#include <ULFramework/ULProcessManager.h>
#include <ULFramework/ULSystemBuilder.h>
#include <ULFramework/ULSystemController.h>
#include <ULFramework/ULFileSystemDatabaseBackend.h>
#include <ULFramework/ULConfigurationPlugin.h>
#include <ULFramework/ULProcess.h>
#include <ULFramework/ULTemplate.h>
#include <ULFramework/ULFrameworkFunctions.h>
#include <ULFramework/ULServerInterface.h>

#endif
