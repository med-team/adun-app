/*
   Project: UL

   Copyright (C) 2005 Michael Johnston & Jordi Villa-Freixa

   Author: Michael Johnston

   Created: 2005-07-12 15:24:33 +0200 by michael johnston

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA.
*/

#ifndef _ULDATABASE_SIMULATIONINDEX_H_
#define _ULDATABASE_SIMULATIONINDEX_H_

#include <Foundation/Foundation.h>
#include <AdunKernel/AdunSimulationData.h>
#include <AdunKernel/AdunFileSystemSimulationStorage.h>
#include "ULFramework/ULDatabaseIndex.h"

/**
ULDatabaseIndex subclass containing extra functionality
to deal with AdSimulationData objects.
\ingroup classes
*/

@interface ULDatabaseSimulationIndex : ULDatabaseIndex
@end


#endif // _ULSIMULATIONDATABASE.M_H_

